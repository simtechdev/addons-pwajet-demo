<?php

/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

/**
 * Gets style packs
 *
 * @return array
 */
function fn_get_packs()
{ 
    $styles_templates_root_dir = Registry::get('config.dir.root') . '/design/backend/css/addons/sd_pwajet/styles_templates/';
    $styles_templates = array_diff(array_slice(scandir($styles_templates_root_dir), 2), ['index.html']);

    $styles = [];

    foreach ($styles_templates as $file_dir) {       
        $styles_manifest_json = @file_get_contents($styles_templates_root_dir . $file_dir . '/manifest.json');
        $styles_manifest = json_decode($styles_manifest_json, true);

        $styles[$file_dir] = $styles_manifest;
    }

    return $styles;
}