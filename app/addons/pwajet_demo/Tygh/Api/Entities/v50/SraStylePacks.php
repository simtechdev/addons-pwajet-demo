<?php
/***************************************************************************
 *                                                                          *
 *   © Simtech Development Ltd.                                             *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 ***************************************************************************/

namespace Tygh\Api\Entities\v50;

use Tygh\Registry;
use Tygh\Api\Response;
use Tygh\Addons\StorefrontRestApi\ASraEntity;

/**
 * Class SraStylePacks
 *
 * @package Tygh\Api\Entities
 */
class SraStylePacks extends ASraEntity
{
    /** @inheritdoc */
    public function index($id = '', $params = [])
    {
        $styles_templates_root_dir = Registry::get('config.dir.root') . '/design/backend/css/addons/sd_pwajet/styles_templates/';
        $styles_templates = array_diff(array_slice(scandir($styles_templates_root_dir), 2), ['index.html']);

        $styles = [];

        $styles_templates = array_filter($styles_templates, function ($value) use ($params) {
            return $value == $params['name'];
        });

        if (empty($params['name'])) {
            foreach ($styles_templates as $file_dir) {
                $styles_manifest_json = @file_get_contents($styles_templates_root_dir . $file_dir . '/manifest.json');
                $styles_manifest = json_decode($styles_manifest_json, true);
    
                $styles[$file_dir] = $styles_manifest;
            }
        } else {
            $file_dir = array_pop($styles_templates);
            $styles_manifest_json = @file_get_contents($styles_templates_root_dir . $file_dir . '/manifest.json');
            $styles_manifest = json_decode($styles_manifest_json, true);

            $styles = $styles_manifest;

            $styles_css = $params['name'] === 'default' ? '' : @file_get_contents($styles_templates_root_dir . $file_dir . '/index.css');
            $styles['content'] = $styles_css;
        }

        return [
            'status' => Response::STATUS_OK,
            'data'   => $styles
        ];
    }

    /** @inheritDoc */
    public function create($params)
    {
        return [
            'status' => Response::STATUS_METHOD_NOT_ALLOWED,
            'data'   => []
        ];
    }

    /** @inheritDoc */
    public function update($id, $params)
    {
        return [
            'status' => Response::STATUS_METHOD_NOT_ALLOWED,
            'data'   => []
        ];
    }

    /** @inheritDoc */
    public function delete($id)
    {
        return [
            'status' => Response::STATUS_METHOD_NOT_ALLOWED,
            'data'   => []
        ];
    }

    /**
     * @inheritDoc
     */
    public function privileges()
    {
        return [
            'index' => true,
        ];
    }

    /**
     * @inheritDoc
     */
    public function privilegesCustomer()
    {
        return [
            'index' => true,
        ];
    }
}
