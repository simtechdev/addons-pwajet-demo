<?php
/***************************************************************************
*                                                                          *
*   © Simtech Development Ltd.                                             *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
***************************************************************************/

defined('BOOTSTRAP') or die('Access denied');

$schema[] = [
    'author'    => 'simtechdev',
    'extension' => 'pwajet_demo',
    'scripts'   => [
        '/js/addons/pwajet_demo/pwajet/1.4.1.ad7aad5e.stylePackRenderSubscriber.js',
        '/js/addons/pwajet_demo/pwajet/1.4.1.6f1392f9.spaDemoBlock.js',
        '/js/addons/pwajet_demo/pwajet/1.4.1.186a562c.spaDemoBlockPropsFactory.js',
    ]   
];

return $schema;
