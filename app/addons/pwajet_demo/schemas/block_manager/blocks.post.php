<?php
/***************************************************************************
*                                                                          *
*   © Simtech Development Ltd.                                             *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
***************************************************************************/

use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

if (Registry::get('addons.sd_pwajet.status') == 'A' 
    && (
        Tygh::$app['addons.sd_pwajet.common']->isSpaLayout() 
        || Tygh::$app['addons.sd_pwajet.common']->isBlockManager()
    )
) {
    $spa_schema = [];
    $spa_schema['spa_demo'] = [
        'content' => [
            'packs' => [
                'type' => 'function',
                'function' => ['fn_get_packs']
            ]
        ],
        'wrappers' => 'blocks/wrappers',
        'cache' => false,
    ];

    if (Tygh::$app['addons.sd_pwajet.common']->isBlockManager()) {
        $schema = $schema + $spa_schema;
    } else {
        $schema = array_merge(Tygh::$app['addons.sd_pwajet.common']->filterSpaBlockTypes($schema), $spa_schema);
    }
}

return $schema;
