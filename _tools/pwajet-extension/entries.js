export default () => ([
  {
    name: 'stylePackRenderSubscriber',
    file: `./src/index.tsx`,
  },
  {
    name: 'spaDemoBlock',
    file: `./src/components/blocks/demo-block/DemoBlock.tsx`
  },
  {
    name: 'spaDemoBlockPropsFactory',
    file: `./src/components/blocks/demo-block/demoBlockPropsFactory.ts`
  },
])
