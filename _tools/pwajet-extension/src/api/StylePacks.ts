import StylePacksRequest from './StylePacksRequest'
import pwajet from 'pwajet'

const client = pwajet.core.api.client

const stylePacks = new StylePacksRequest(client.getClient(), client.getConfig())

export const getPack = (name: string) =>
  stylePacks.name(name).get()

export const stylePacksApi = {
  getPack,
}
