interface ICsCartStylePack {
  name: string
  description: string
}

export interface IStylePack {
  name:           string;
  description:    string;
}

export const castPack = async (pack: ICsCartStylePack): Promise<IStylePack> => {
  return {
    name: pack.name,
    description: pack.description,
  }
}
