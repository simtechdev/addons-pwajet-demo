import pwajet from 'pwajet'

const AbstractRequest = pwajet.core.api.AbstractRequest
const clientApi = pwajet.core.api.client

class StylePacksRequest extends AbstractRequest {
  entityPath = 'style_packs'
  prefix = 'sra_'
  handlerParams: any
  params: any

  constructor(
    client: ReturnType<typeof clientApi.getClient>,
    config: ReturnType<typeof clientApi.getConfig>
  ) {
    super(client, config)
    this.handlerParams = {}
    this.params = {}
  }

  protected buildUrl(): string {
    let url = super.buildUrl()
    url = url + (this.handlerParams.name ? `?name=${this.handlerParams.name}` : '')

    return url
  }

  name(name: string) {
    this.initParams()

    this.handlerParams.name = name
    return this
  }

  protected initParams(): void {
    this.params = {
      ...this.params,
    }
  }
}

export default StylePacksRequest
