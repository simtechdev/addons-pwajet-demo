import React from 'react'
import { IProps } from './IStylePack'

const StylePack: React.FC<IProps> = (props) => {
  const { currentPackContent } = props

  return currentPackContent ? (
    <style dangerouslySetInnerHTML={{ __html: currentPackContent }}></style>
  ) : null
}

export default StylePack
