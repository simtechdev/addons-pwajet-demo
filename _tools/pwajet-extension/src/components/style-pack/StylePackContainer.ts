import { connect } from 'react-redux'
import StylePack from './StylePack'
import { IStateProps } from './IStylePack'
import { StateWithStylePacksState } from '../../redux/types/StylePacksState'

function mapStateToProps(state: StateWithStylePacksState): IStateProps {
  return {
    currentPackContent: state.StylePacks.currentPackContent,
  }
}

export default connect<IStateProps, null, null, StateWithStylePacksState>(
  mapStateToProps,
  null
)(StylePack)
