import React from 'react'
import '@testing-library/jest-dom'
import { render } from '@testing-library/react'
import { IProps } from './IStylePack'
import StylePack from './StylePack'

jest.mock(
  'pwajet',
  () => {
    return {
      core: {
        utils: {
          storage: {
            localStorage: {
              set: jest.fn(),
            },
          },
        },
      },
    }
  },
  { virtual: true }
)

const setup = (customProps?: Partial<IProps>) => {
  const props = {
    ...{
      currentPackContent: '',
    },
    ...customProps,
  }

  return {
    wrapper: render(<StylePack {...props} />),
    props: props,
  }
}

describe('DemoTools component', () => {
  it('should render defaults', () => {
    const { wrapper } = setup()
    expect(
      wrapper.container.getElementsByTagName('style').item(0)
    ).not.toBeInTheDocument()
  })

  it('should apply pack styles', () => {
    const { wrapper } = setup({ currentPackContent: '.b-block { color: red; }' })

    expect(
      wrapper.container.getElementsByTagName('style').item(0)
    ).toBeInTheDocument()
  })
})
