export interface IStateProps {
  currentPackContent: string
}

export type IProps = IStateProps
