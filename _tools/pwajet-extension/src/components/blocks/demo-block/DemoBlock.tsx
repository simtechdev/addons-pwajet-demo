import React from 'react'
import { IProps } from './IDemoBlock'
import pwajet from 'pwajet'
import translations from '../../../intl/translations'

pwajet.core.translations.injectTranslations(translations)

const DemoTools = React.lazy(() =>
  pwajet.core.store.dynamicImportComponentWithRedux('stylePacks')(
    import('../../demo-tools/DemoToolsContainer'),
    import('../../../redux/reducers/stylePacksReducer'),
    import('../../../redux/epics/stylePacksEpics')
  )
)

const DemoBlock: React.FC<IProps> = (props) => {
  const { packList } = props

  return (
    <React.Suspense fallback={null}>
      <DemoTools packList={packList} />
    </React.Suspense>
  )
}

export default DemoBlock
