import { IStylePack } from '../../../api/parser/StylePack'
import { CoreTypes } from 'pwajet'

export interface IOwnProps extends CoreTypes.Components.BaseBlockProps {
  packList: Array<IStylePack>
}

export type IProps = IOwnProps
