import pwajet from 'pwajet'
import { castPack } from '../../../api/parser/StylePack'

const castListItems = pwajet.core.api.castListItems

const demoBlockPropsFactory =  async (data: any) => {
  return {
    packList: await castListItems(data.content.packs, castPack),
  }
}

export default demoBlockPropsFactory
