import { connect } from 'react-redux'
import DemoTools from './DemoTools'
import { IStateProps, IDispatchProps, IOwnProps } from './IDemoTools'
import { requestStylePackContent, StylePacksAction } from '../../redux/actions/StylePacksAction'
import { StateWithStylePacksState } from '../../redux/types/StylePacksState'
import { Dispatch } from 'react'

function mapStateToProps(state: StateWithStylePacksState): IStateProps {
  return {
    isRequesting: state.StylePacks.isRequesting,
    currentPackName: state.StylePacks.currentPackName,
  }
}

function mapDispatchToProps(dispatch: Dispatch<StylePacksAction>): IDispatchProps {
  return {
    requestStylePackContent: name => dispatch(requestStylePackContent(name)),
  }
}

export default connect<IStateProps, IDispatchProps, IOwnProps, StateWithStylePacksState>(
  mapStateToProps,
  mapDispatchToProps
)(DemoTools)
