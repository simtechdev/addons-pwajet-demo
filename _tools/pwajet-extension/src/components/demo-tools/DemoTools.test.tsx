import React from 'react'
import '@testing-library/jest-dom'
import { render, fireEvent } from '@testing-library/react'
import useIntl from 'react-intl/src/components/useIntl'
import { IProps } from './IDemoTools'
import DemoTools from './DemoTools'

jest.mock(
  'pwajet',
  () => {
    return {
      core: {
        components: {
          Button: (props: {
            className: string
            children: React.ReactElement
            onClick: (event: React.MouseEvent) => void
          }) => {
            const { onClick, children, className } = props
            return (
              <button onClick={onClick} className={className} data-testid='button'>
                {children}
              </button>
            )
          },
          Icon: () => {
            return 'Icon'
          },
          Select: (props: {
            name: string
            description: string
            value: string
            options: Array<{ value: string; label: string }>
            onChange: (event: React.ChangeEvent) => void
            disabled: boolean
          }) => {
            const { options, name, value, onChange, disabled } = props
            return (
              <select
                name={name}
                value={value}
                onChange={onChange}
                disabled={disabled}
                data-testid='select'
              >
                {options.map(({ value, label }) => (
                  <option value={value} key={label}>
                    {label}
                  </option>
                ))}
              </select>
            )
          },
          animations: {
            CSSTransition: (props: {
              in: boolean
              children: React.ReactElement
              unmountOnExit: boolean
              mountOnEnter: boolean
            }) =>
              props.unmountOnExit && props.mountOnEnter
                ? props.in
                  ? props.children
                  : null
                : props.children,
          },
        },
        utils: {
          storage: {
            localStorage: {
              set: jest.fn(),
            },
          },
        },
      },
    }
  },
  { virtual: true }
)
jest.mock('react-intl/src/components/useIntl')

beforeEach(() => {
  // @ts-ignore
  useIntl.mockReturnValue({
    formatMessage: ({ defalutMessage }: any) => defalutMessage,
  })
})

const setup = (customProps?: Partial<IProps>) => {
  const props = {
    ...{
      packList: [
        { name: 'default', description: 'Default Styles' },
        { name: 'dark', description: 'Dark' },
        { name: 'blue', description: 'Blue' },
      ],
      currentPackName: 'default',
      isRequesting: false,
      requestStylePackContent: jest.fn(),
    },
    ...customProps,
  }

  return {
    wrapper: render(<DemoTools {...props} />),
    props: props,
  }
}

describe('DemoTools component', () => {
  it('should render defaults', () => {
    const { wrapper } = setup()
    expect(wrapper.container.querySelector('.b-sd-demo-tools')).toBeInTheDocument()
  })

  it('should render popup container', () => {
    const { wrapper } = setup()

    fireEvent.click(wrapper.getByTestId('button'))
    expect(
      wrapper.container.querySelector('.b-sd-demo-tools__popper')
    ).toBeInTheDocument()

    fireEvent.click(wrapper.getByTestId('button'))
    expect(
      wrapper.container.querySelector('.b-sd-demo-tools__popper')
    ).not.toBeInTheDocument()
  })

  it('should change selected value', () => {
    const { wrapper } = setup()

    fireEvent.click(wrapper.getByTestId('button'))
    const select = wrapper.getByTestId('select') as HTMLSelectElement
    expect(select.value).toEqual('default')

    fireEvent.change(select, { target: { value: 'dark' } })
    expect(select.value).toEqual('dark')
  })
})
