import { IStylePack } from '../../api/parser/StylePack'

export interface IOwnProps {
  packList: Array<IStylePack>
}

export interface IStateProps {
  isRequesting:       boolean
  currentPackName:    string
}

export interface IDispatchProps {
  requestStylePackContent: (name: string) => void
}

export type IProps = IOwnProps & IStateProps & IDispatchProps
