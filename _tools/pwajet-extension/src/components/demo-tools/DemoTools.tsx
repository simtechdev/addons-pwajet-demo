import React from 'react'
import { IProps } from './IDemoTools'
import './DemoTools.css'
import { Settings as SettingsIcon } from 'react-feather'
import pwajet from 'pwajet'
import { useIntl } from 'react-intl'
import messages from './DemoTools.messages'
import LocalStorageKeys from '../../constants/LocalStorageKeys'

const Button = pwajet.core.components.Button
const Icon = pwajet.core.components.Icon
const Select = pwajet.core.components.Select
const CSSTransition = pwajet.core.components.animations.CSSTransition
const localStorage = pwajet.core.utils.storage.localStorage

const DemoTools: React.FC<IProps> = (props) => {
  const { packList, requestStylePackContent, currentPackName, isRequesting } = props

  const [isOpen, setIsOpen] = React.useState(false)
  const [selectedPackName, setSelectedPackName] = React.useState(currentPackName)
  const intl = useIntl()

  React.useEffect(() => {
    requestStylePackContent(selectedPackName)
    localStorage.set(LocalStorageKeys.PACK_NAME, selectedPackName)
  }, [requestStylePackContent, selectedPackName])

  const handleClick = () => {
    setIsOpen(!isOpen)
  }

  const handleStyleChange = (
    event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>
  ) => {
    setSelectedPackName(event.target.value as string)
  }

  return (
    <>
      <div className='b-sd-demo-tools'>
        <CSSTransition
          in={isOpen}
          classNames={{
            enterDone: 'b-sd-demo-tools__button--active',
          }}
        >
          <Button onClick={handleClick} className='b-sd-demo-tools__button'>
            <Icon icon={SettingsIcon} width={40} height={40} />
          </Button>
        </CSSTransition>

        <CSSTransition
          in={isOpen}
          timeout={200}
          unmountOnExit
          mountOnEnter
          classNames={{
            enter: 'animated fadeIn',
            exit: 'animated fadeOut',
          }}
        >
          <div className='b-sd-demo-tools__popper'>
            <div className='b-sd-demo-tools__container'>
              <React.Suspense fallback={null}>
                <Select
                  name='style'
                  description={intl.formatMessage({ ...messages.stylePackLabel })}
                  value={selectedPackName}
                  options={packList.map(({ name, description }) => {
                    return {
                      value: name,
                      label: description,
                    }
                  })}
                  onChange={handleStyleChange}
                  disabled={isRequesting}
                />
                </React.Suspense>
            </div>
          </div>
        </CSSTransition>
      </div>
    </>
  )
}

export default DemoTools
