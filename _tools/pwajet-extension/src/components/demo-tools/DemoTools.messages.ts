import { defineMessages  } from 'react-intl'

const messages = {
  stylePackLabel: {
    id: 'app.demo-tools.select-style-pack.label',
    defalutMessage: 'Style pack',
  },
}

export default defineMessages(messages)
