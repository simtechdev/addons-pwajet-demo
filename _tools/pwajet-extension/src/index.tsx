import pwajet from 'pwajet'
import React from 'react'

const renderSubscriber = pwajet.core.renderSubscriber
const localStorage = pwajet.core.utils.storage.localStorage

const StylePack = React.lazy(() =>
  pwajet.core.store.dynamicImportComponentWithRedux('stylePacks')(
    import('./components/style-pack/StylePackContainer'),
    import('./redux/reducers/stylePacksReducer'),
    import('./redux/epics/stylePacksEpics')
  )
)

renderSubscriber.on(
  'render-element.screen-handler/ScreenHandler',
  (subscriber) => {

    subscriber.appendTo('.b-screen-handler__wrapper', () => (
      <React.Suspense fallback={null}>
        <StylePack />
      </React.Suspense>
    ))
  })
