import { CoreTypes } from 'pwajet'

interface StylePacksState {
  isRequesting: boolean
  currentPackName: string
  currentPackContent: string
  error?: CoreTypes.ApiError
}

export type StateWithStylePacksState = {
  StylePacks: StylePacksState
}

export default StylePacksState
