import StylePacksActionTypes from './StylePacksActionTypes'
import { CoreTypes } from 'pwajet'

export interface RequestStylePackContent {
  type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT
  payload: {
    name: string
  }
}

export function requestStylePackContent(name: string): RequestStylePackContent {
  return {
    type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT,
    payload: {
      name,
    },
  }
}

export interface RequestStylePackContentSuccess {
  type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_SUCCESS
  payload: {
    currentPackContent: string
  }
}

export function requestStylePackContentSuccess(
  currentPackContent: string
): RequestStylePackContentSuccess {
  return {
    type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_SUCCESS,
    payload: {
      currentPackContent,
    },
  }
}

export interface RequestStylePackContentFailure {
  type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_FAILURE
  payload: {
    error: CoreTypes.ApiError
  }
}

export function requestStylePackContentFailure(
  error: CoreTypes.ApiError
): RequestStylePackContentFailure {
  return {
    type: StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_FAILURE,
    payload: {
      error,
    },
  }
}
