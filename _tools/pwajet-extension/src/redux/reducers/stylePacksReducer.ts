import pwajet from 'pwajet'
import StylePacksState, { StateWithStylePacksState } from '../types/StylePacksState'
import { StylePacksAction } from '../actions/StylePacksAction'
import StylePacksActionTypes from '../actions/StylePacksActionTypes'
import LocalStorageKeys from '../../constants/LocalStorageKeys'

const localStorage = pwajet.core.utils.storage.localStorage

pwajet.core.registerIsLoading<StateWithStylePacksState>(
  state => state.StylePacks?.isRequesting
  || false
)

export const initialState: StylePacksState = {
  isRequesting: false,
  currentPackName: localStorage.get(LocalStorageKeys.PACK_NAME) || 'default',
  currentPackContent: '',
}

export const stylePacksReducer = (
  state: StylePacksState = initialState,
  action: StylePacksAction
  ): StylePacksState => {
  switch (action.type) {
    case StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT: {
      return {
        ...state,
        isRequesting: true,
        error:        undefined,
        currentPackName: action.payload.name,
      }
    }

    case StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_SUCCESS: {
      return {
        ...state,
        isRequesting: false,
        currentPackContent: action.payload.currentPackContent,
      }
    }

    case StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT_FAILURE: {
      return {
        ...state,
        isRequesting: false,
        error:        action.payload.error,
      }
    }

    default:
      return state
  }
}
