import { map, switchMap, catchError, retryWhen, delay, mergeMap } from 'rxjs/operators'
import { throwError, defer, from, of } from 'rxjs'
import { ActionsObservable, ofType } from 'redux-observable'

import {
  RequestStylePackContent,
  requestStylePackContentFailure,
  requestStylePackContentSuccess,
} from '../actions/StylePacksAction'
import StylePacksActionTypes from '../actions/StylePacksActionTypes'
import { stylePacksApi } from '../../api/StylePacks'

const SUBMIT_RETRY_TIMEOUT_IN_MILLISECONDS = 5000

/**
 * Request Style Pack
 */
export const requestStylePackContentEpic = (
  action$: ActionsObservable<RequestStylePackContent>
) =>
  action$.pipe(
    ofType(
      StylePacksActionTypes.REQUEST_STYLE_PACK_CONTENT
    ),
    switchMap(action =>
      defer(
        () => stylePacksApi.getPack(action.payload.name)
      ).pipe(
        retryWhen(errors => errors.pipe(
          mergeMap(error => {
            if (error.status !== 0) {
              return throwError(error)
            }

            return [error]
          }),
          delay(SUBMIT_RETRY_TIMEOUT_IN_MILLISECONDS)
        )),
        mergeMap((result: any) => of(requestStylePackContentSuccess(result.data.content))),
        catchError((error) => {
          return from(import('@pwajet/entities/error/factories/apiErrorFactory')).pipe(
            map(module => module.default),
            map(createApiError =>
              requestStylePackContentFailure(
                createApiError({
                  status: error.status,
                  message: error.messages || error.message,
                })
              )
            )
          )
        })
      )
    )
  )

export const stylePacksEpics = [
  requestStylePackContentEpic,
]
