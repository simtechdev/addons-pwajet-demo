<?php
/**
 * The add-on creates symbolic links from current directory
 * To delete symbolic links, add "delete" to the end of the request
 */
if (empty($argv[1])) {
    echo 'You didn\'t specify the target directory, use: php create_symlinks.php ../../435 [ delete ]' . PHP_EOL;
    exit(127);
}

$paths = array(
    '/app/addons/pwajet_demo' => '/app/addons/pwajet_demo',
    '/design/backend/media/images/addons/pwajet_demo' => '/design/backend/media/images/addons/pwajet_demo',
    '/var/langs/en/addons/pwajet_demo.po' => '/var/langs/en/addons/pwajet_demo.po',
    '/var/langs/ru/addons/pwajet_demo.po' => '/var/langs/ru/addons/pwajet_demo.po',
    '/js/addons/pwajet_demo' => '/js/addons/pwajet_demo',
);

$dir_target = realpath($argv[1]);

$dir = realpath(dirname(__FILE__) . '/../');

foreach ($paths as $target => $source) {

    if (file_exists($dir_target . $target)) {
        unlink($dir_target . $target);
    }
    if (!isset($argv[2]) || $argv[2] != 'delete') {
          // WIN
        if (DIRECTORY_SEPARATOR == '\\') {
            $dirSrc = $dir . str_replace('/', '\\', $source);
            $dirDest = $dir_target . str_replace('/', '\\', $target);
            exec('mklink /D ' . '"' . $dirDest . '"' . ' ' . '"' .  $dirSrc . '"' . ' ');
        }
        // NIX
        else {
            system("ln -s {$dir}{$source} {$dir_target}{$target}");
        }
    }
}